var express = require('express')
var bodyParser=require('body-parser') // Para pasar parámetros por el Body
// Variables para la conexión con MongoDB.
var MongoClient = require('mongodb').MongoClient // Para utilizar Mongodb
var assert = require('assert')
//var cors = require('cors')
var app = express () //  Inicializo una variable de tipo objeto express
app.use(bodyParser.json()) // Para utilizar los Bodys que utilice json
var requestJson=require('request-json')

var urlMlabRaiz="https://api.mlab.com/api/1/databases/bdbancomas/collections"
var apiKey="apiKey=JSsMVObj-Drbac9-wPs-oCBTfikv18qf"
var clienteMlab = null

var url = 'mongodb://admin:altesi123@ds237868.mlab.com:37868/bdbancomas';

var totusu = 0

//app.use(cors())



app.get ('/apitechu/v5/usuarios', function (req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body){
    if (!err) {
         res.send(body)
    }
  })
})

app.get ('/apitechu/v5/usuarios/:id', function (req, res) {
  var id = req.params.id
  var query = 'q={"id":' + id + '}&f={"first_name":1, "last_name":1, "_id":0}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      if (body.length > 0){
        res.send(body[0])
        console.log(body[0])
         //res.send( {"nombre " : body[0].first_name, "apellido " : body[0].last_name })
       }
         else {
           res.status(404).send ('Usuario no encontrado')
         }
       }
    }
  )}
)

app.post('/apitechu/v5/login', function(req, res) {
   var email = req.headers.email
   var password = req.headers.password
   var query = 'q={"email":"' + email + '","password":"' + password + '"}'
   clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
   console.log(clienteMlab)
   clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1) // login ok
         {
           clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
           var cambio = '{"$set":{"logged":true}}'
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP,resP,bodyP){
            res.send({"login":"ok", "id":body[0].id, "nombre":body[0].first_name, "apellidos":body[0].last_name})
          }) //Este último campo es el Body del PUT
          // Lo mismo que para el POST y el DELETE

       }
       else {
         res.status(404).send('Usuario no encontrado')
       }
     }
   })
})

app.post('/apitechu/v5/logout', function(req, res) {
   var id = req.headers.id
   var query = 'q={"id":' + id + ', "logged": true }'
   clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
   console.log(clienteMlab)
   clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1) // Estaba logado
         {
           clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
           var cambio = '{"$set":{"logged":false}}'
          clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP,resP,bodyP){
            res.send({"logout":"ok", "id":body[0].id })
          }) //Este último campo es el Body del PUT
          // Lo mismo que para el POST y el DELETE

       }
       else {
         res.status(200).send('Usuario no logado previamente')
       }
     }
   })
})






         MongoClient.connect(url, function(err, db) {
           assert.equal(null, err)
           console.log("Conectado al server");
           var leerusuarios = function(db, callback) {
             // Carga de la colección de películas.
             var collection = db.collection('usuarios')
             // Consulta de los documentos de usuarios
             collection.find({}).toArray(function(err, res) {
               assert.equal(err, null)
               //assert.equal(3, res.length)
               //console.log("Estos son los usuarios")
               //console.dir(res) // Aquí muestra todos los usuarios que están en "res"
               totusu=res.length //obtengo el número total de usuarios en la BBDD
               console.log("Total de usuarios: ", totusu)
               callback(res)
             })
           }
             db.close();
             // Conexión con el servidor para la consulta de datos
             MongoClient.connect(url, function(err, db) {
               assert.equal(null, err)
               console.log("Conexión el servidor para las operaciones CRUD")

               // Invocación encadenada de las operaciones.
               leerusuarios(db, function() {
                 db.close()
                 })
             })
        })


app.post ('/apitechu/v5/altausuario', function(req, res) {

          var nombre = req.headers.nombre
          var apellido = req.headers.apellido
          var correo = req.headers.correo
          var pclave = req.headers.pclave

          totusu ++

          var insertaUsuario = function(db, callback) {
            // Carga de la colección de películas.
            var collection = db.collection('usuarios')

            // Inserción del usuario
            collection.insert([{id: totusu, first_name: nombre, last_name: apellido, email: correo, password: pclave}], function(err, docs) {
              // Tests unitarios
              assert.equal(err, null)
              assert.equal(1, docs.result.n)
              assert.equal(1, docs.ops.length)

              // Log de consola
              console.log("Insertado usuario en las colección de usuarios.")

              callback(docs)
            })
          }
          var insertaCuenta = function(db,callback){
            var collection = db.collection('cuentas')
            collection.insert([{iban:"ES1234 1234 1234 1234 1234",idcliente: totusu, movimientos:[{id:1,fecha:"2018/04/12",importe:0, moneda:"EUR"}],saldo:0}], function(err, docs){
              assert.equal(err,null)
              assert.equal(1, docs.result.n)
              assert.equal(1, docs.ops.length)
              console.log ("Insertado cuenta en la colección de cuentas")

              callback(docs)
            })
          }
          // Conexión con el servidor para la consulta de datos
          MongoClient.connect(url, function(err, db) {
            assert.equal(null, err)
            console.log("Conexión con el servidor para las operaciones CRUD")

            // Invocación encadenada de las operaciones.
            if (nombre != null) {
              insertaUsuario(db, function() {
                db.close()
              })
              insertaCuenta(db,function() {
                db.close()
              })
            }
          })
          res.send({"Usuario dado de alta ":"ok"})
        })

        app.get ('/apitechu/v5/cuentas', function(req, res) {
           var id = req.headers.idcliente
           var query = 'q={"idcliente":' + id + ' }'
           var filter = 'f={"iban":1,"saldo":1,"_id":0}'
           clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter +"&"+ apiKey)
           console.log(clienteMlab)
           clienteMlab.get('', function(err, resM, body) {
             if (!err) {
                 res.send({"id": id, "iban": body})
                    }//Este último campo es el Body del PUT
                  // Lo mismo que para el POST y el DELETE
               }
             )
        })


           app.get ('/apitechu/v5/movimientos', function(req, res) {
              var iban = req.headers.iban
              var query = 'q={"iban": "' + iban + '"}'
              var filter = 'f={"movimientos":1,"_id":0}'
              clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
              clienteMlab.get('', function(err, resM, body) {
                if (!err) {
                      clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas")
                      res.send({"iban": iban, "movimientos": body })
                     }
                     //Este último campo es el Body del PUT
                     // Lo mismo que para el POST y el DELETE

                  }
                )
              })
        app.get ('/apitechu/v5/petimovimiento', function(req, res) {

                  var iban = req.headers.iban
                  var query = 'q={"iban": "' + iban + '" }'
                  var filter = 'f={"saldo":1,"_id":0}'
                  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter +"&"+ apiKey)
                  console.log(clienteMlab)
                  clienteMlab.get('', function(err, resM, body) {
                    if (!err) {
                        clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas")
                        res.send({"saldo":body[0].saldo})
                           }//Este último campo es el Body del PUT
                         // Lo mismo que para el POST y el DELETE
                      }
                    )
               })

        app.post ('/apitechu/v5/actualizamovimientos', function (req, res){
          var iban = req.headers.iban
          var saldodest=req.headers.saldo
          console.log("iban:",iban, "Saldo:",saldodest)
          var query = 'q={"iban": "' + iban + '"}'
          //var filter = 'f={"saldo":1,"_id":0}'
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
          console.log(clienteMlab)
                var cambiasaldo = {"$set":{"saldo":saldodest}}
                console.log (cambiasaldo)
                //console.log("Salida segundo clienteMlab: ", clienteMlab)
                clienteMlab.put('',cambiasaldo, function(errP,resP,bodyP){
                res.send({"saldo modificado":"ok"})
                console.log("Saldo modificado")
               })
          })


          app.post ('/apitechu/v5/insertanuevomovimiento', function (req,res){
            var iban = req.headers.iban
            var id = req.headers.id
            var fecha = req.headers.fecha
            var importe = req.headers.importe
            console.log("iban:",iban, "id movimiento", id)
            var query = 'q={"iban": "' + iban + '"}'
            var filter = 'f={"movimientos":1,"_id":0}'
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
            console.log(clienteMlab)
            clienteMlab.get('', function(err, resM, body) {
              if (!err) {
                    var instmovimiento = {"id":id,"fecha":fecha,"importe":importe,"moneda":"EUR"}
                    clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
                    body[0].movimientos.push(instmovimiento)
                    var cambio = {"$set": {"movimientos":body[0].movimientos}};
                    clienteMlab.put('', cambio, function(err, resP, bodyP) {
                    res.send({"movimientos":body[0].movimientos})
                    })
                    }

                  })
                   //Este último campo es el Body del PUT
                   // Lo mismo que para el POST y el DELETE
                  console.log("Nuevo movimiento insertado")
                //})

          })





        //var cambio = '{"$set":{"logged":false}}'
        //clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP,resP,bodyP){



var port = process.env.PORT || 3000 // Defino el puerto

var fs=require ('fs'); // Para el manejo de ficheros

var usuarios = require ('./usuarios.json')
var usu = require('./fichusuarios.json')

var cuentas = require('./cuentas.json')


app.listen(port) //pongo el puerto en escucha

console.log("API escuchando en el puerto " + port)

//console.log ("hola mundo")



app.get ('/apitechu/v1', function(req,res) {
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi API"})
})
app.get ('/apitechu/v1/usuarios', function (req, res) {
  res.send(usuarios)
})


  app.post ('/apitechu/v2/usuarios', function (req, res) {
    //var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country}
    var nuevo=req.body // Aquí nos tiene que llegar un body en json
    usuarios.push(nuevo) // HAcer una ingesta de un nuevo registro en el fichero
    //console.log(req.headers)
    const datos = JSON.stringify (usuarios) // Para convertir a cadena
    // Esta es la escritura en el fichero
    fs.writeFile ("./usuarios.json", datos, "utf8", function(err) {
      if (err) {
          console.log (err)
        }
        else {
      console.log ("Fichero guardado")
    }
    })


  res.send("Alta OK")
})
app.delete('/apitechu/v1/usuarios/:id', function (req, res) { // Eliminar una entrada en el fichero por ID
   usuarios.splice(req.params.id-1, 1) // A partir del indice que yo te paso, cuantos elementos eliminas
                    // Este es el indice que le paso
   res.send ("Usuario Borrado")
 })

 app.post ('/apitechu/v1/monstruo/:p1/:p2', function (req,res) {
   console.log ("Parametros")
   console.log(req.params)
   console.log("Query Strings")
   console.log(req.query)
   console.log("Headers")
   console.log(req.headers)
   console.log("Body")
   console.log(req.body)
 })

 app.post ('/apitechu/v3/fichusuarios/login', function (req,res) {
      var email=req.headers.email
      var password=req.headers.password
      var idusuario=0
      for (var i = 0; i < usu.length; i++) {
        if (usu[i].email == email && usu[i].password == password ) {
          idusuario= usu[i].id
          usu[i].logged = true
          break
        }
      }
      if (idusuario !=0 ){
          res.send ( {"encontrado":"si","id":idusuario})
      }
      else {
        res.send ( {"encontrado":"no"})
      }
 })

app.post ('/apitechu/v3/fichusuarios/logout', function (req,res) {
    var idusuario = req.headers.id

    for (var i = 0; i < usu.length; i++) {
      if (usu[i].id == idusuario ) {
            res.send ( { "encontrado": "OK", "usuID": usu[i].id, "nombre": usu[i].first_name })
        usu[i].logged = false
        break
      }
    }

})

// BLOQUE CUENTAS  ***********************************************


app.get ('/apitechu/v4/cuentas', function (req,res) {
    res.send (cuentas)
})

app.get ('/apitechu/v4/movimientos', function (req, res) {
    var ibanId = req.headers.iban
    var arraymovi =[]
    for (var i = 0; i < cuentas.length; i++) {
      if (cuentas[i].iban == ibanId ) {
        arraymovi.push(cuentas[i].movimientos)
        //res.send (cuentas[i].movimientos)
      }
    }
    res.send(arraymovi)
})

app.get ('/apitechu/v4/cliente', function (req, res) {
  var clienteId = req.headers.idcliente
  var arrayIban=[]
  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].idcliente == clienteId) {
      arrayIban.push(cuentas[i].iban)
      //res.send ( {"el cliente": cuentas[i].idcliente, "tiene como numero de cuenta": cuentas[i].iban })
    }
  }
  res.send( {"El cliente": clienteId, "tiene como numero de cuentas": arrayIban })
  /*for (var i = 0; i < arrayIban.length; i++) {
    res.send( {"El cliente": clienteId, "Tiene como numero de cuenta": arrayIban[i] })*/
  })
  //res.send ( {"el cliente": cuentas[i].idcliente, "tiene como numero de cuenta": cuentas[i].iban })
