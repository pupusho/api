
var mocha = require('mocha')
var chai = require('chai')
var chaihttp = require('chai-http')
var server = require('../server')

var should = chai.should() // Validar lo que tu esperas

chai.use(chaihttp) //configurae CHAI con módule http

describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request ('http://www.google.es')
    .get ('/')
    .end((err, res) => {
      //console.log(res)
      res.should.have.status(200) // Debería de tener status valor 200
      done()
    })
  })
})
describe('Tests de API usuarios', () => {
  it('Raíz OK', (done) => {
    chai.request ('http://localhost:3000')
    .get ('/apitechu/v1')
    .end((err, res) => {
      //console.log(res)
      res.should.have.status(200) // Debería de tener status valor 200
      res.body.mensaje.should.be.eql ("Bienvenido a mi API") /* Aquí busco en el body de
           app.get (/apitechu/v1) del fichero server.js, que el mensaje sea igual al
           que he puesto dentro del  () */
      done()
    })
  })
  it('Lista de usuarios', (done) => {
    chai.request ('http://localhost:3000')
    .get ('/apitechu/v1/usuarios')
    .end((err, res) => {
      //console.log(res)
      res.should.have.status(200) // Debería de tener status valor 200
      res.body.should.be.a ("array") /* El tipo de dato devuelve un array */
      for (var i = 0; i < res.body.length; i++) {
        res.body[i].should.have.property('email')
        res.body[i].should.have.property('password') /* Con esto compruebo que cada
        entrada del array de la lista del fichero usuarios.json tengan la propiedad
        email y password */
      }
      done()
    })
  })
})
